<html>
	<head>
		<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>404 Page Not Found</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @section('meta')@show

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #B0BEC5;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 30px;
				margin-bottom: 40px;
			}

			.title.h1{
				font-size: 70px;
				margin-bottom: 40px;
			}

			.title a{
				font-size: 20px;
				margin-bottom: 40px;
				color: #B0BEC5;
			}

			.title a:hover{
				color: #000;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title h1">404</div>
				<div class="title">Url not found</div>
				<div class="title"><a href="/">Quay lại trang chủ</a></div>
			</div>
		</div>
	</body>
</html>
