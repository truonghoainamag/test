<?php
    use App\Model\Category;
?>
@extends('backend.index')
@section('title')
    Danh mục
@endsection
@section('content')
    <div class="col-md-12 class_title">
        <h3><i class="fa fa-folder-open"></i> Danh mục</h3>
    </div>
    <div class="col-md-2 thn_col4">
        <span class="pull-left"><a href="{{route('category_create_path')}}" class="btn btn-success">Thêm mới</a></span>
    </div>
    <div class="col-md-6">
        <div class="col-md-3"><button class="btn btn-success">Xóa tất cả</button></div>
        <div class="col-md-3"><button class="btn btn-success">Hiện tất cả</button></div>
        <div class="col-md-3"><button class="btn btn-success">Ẩn tất cả</button></div>
    </div>
    <div class="col-md-4 thn_col4">
        <form>
            <span class="pull-right">
                <div class="col-md-12 class_search_1" style="margin-left: 0px;">
                    <div class="col-md-9 class_test_search" style="margin-left: 0px;"><input type="text" class="form-control pull-left" name="search" placeholder="Search" value="{{@$search}}"></div>
                    <div class="col-md-3 class_test_search1" style="margin-left: 0px;">
                        <button type="submit" class="btn btn-success pull-right">Search</button>
                    </div>
                </div>
            </span>
        </form>
    </div>
    <div class="clearfix thn_title"></div>
    <div class="col-md-12 class_content">
        <div class="table-responsive">
            <table class="table table-striped table-primary table-bordered class_table">
                <thead>
                <tr>
                    <th class="text-center border-right" style="width: 1%;"><input type="checkbox" class="select-all" ></th>
                    <th class="text-center border-right" style="width: 1%;">No.</th>
                    <th>Name</th>
                    <th>Parent</th>
                    <th>Position</th>
                    <th>Status</th>
                    <th>Created date</th>
                    <th class="text-center" style="width: 12%;">Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($category) && count($category) > 0)
                    <?php $no=1; ?>
                    @foreach($category as $cate)
                        @if($cate['category_position']==1)
                            <?php $position='Bottom'; ?>
                        @elseif($cate['category_position']==0)
                            <?php $position='Top'; ?>
                        @elseif($cate['category_position']==2)
                            <?php $position='Left'; ?>
                        @elseif($cate['category_position']==3)
                            <?php $position='Right'; ?>
                        @endif
                        <tr>
                            <td class="text-center"><input type="checkbox"  name="id[]" value="{{$cate['category_id']}}"></td>
                            <td class="text-center border-right">{{$no}}</td>
                            <td>
                                <span>{{$cate['category_name']}}</span>
                            </td>
                            <td>
                                <?php $cate_id=Category::getListId($cate['category_parent']); ?>
                                <span>
                                    @if(count($cate_id) > 0 )
                                    {{$cate_id['category_name']}}
                                    @else
                                        {{'NULL'}}
                                    @endif
                                    </span>
                            </td>
                            <td class="text-center">{{$position}}</td>
                            <td class="text-center">
                                @if($cate['category_status']==1)
                                    <i class="fa fa-check-square-o fa-lg" style="color:#4cae4c;"></i>
                                @else
                                    <i class="fa fa-check-square-o fa-lg"></i>
                                @endif
                            </td>
                            <td>{{$cate['category_created_at']}}</td>
                            <td class="text-center">
                                <a href="{{route('category_edit_path',$cate['category_id'])}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a onclick="comfirm_delete('{{route('category_destroy_path',$cate['category_id'])}}')" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php $no++; ?>
                    @endforeach
                @else
                    <tr>
                        <td colspan="8" class="text-center">Danh sách rổng</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
        @if(isset($category))
            @if(count($category)>0)
                <nav class="pull-right">
                    <ul class="pagination pagination_trang">
                        <?php ?>
                        @if($category->currentPage()!=1)
                            <li>
                                <a href="{{$category->url($category->perPage())}}">
                                    <i class="fa fa-angle-double-left"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$category->url($category->currentPage()-1)}}">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                            </li>
                        @endif
                        @for($i=1;$i<=$category->lastPage();$i++)
                            <li class="{{($category->currentPage()==$i) ? 'active':''}}">
                                <a href="{{$category->url($i)}}">{{$i}}</a>
                            </li>
                        @endfor
                        @if($category->currentPage()!=$category->lastPage())
                            <li>
                                <a href="{{$category->url($category->currentPage()+1)}}">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                            <li>
                                <a href="{{$category->url($category->lastPage())}}">
                                    <i class="fa fa-angle-double-right"></i>
                                </a>
                            </li>
                        @endif
                    </ul>
                </nav>
            @endif
        @endif
    </div>
@endsection