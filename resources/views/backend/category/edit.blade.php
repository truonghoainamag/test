@extends('backend.index')
@section('title')
    Sửa danh mục
@endsection
@section('content')
    <h3><i class="fa fa-folder-open"></i> Sửa danh mục</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" action="{{route('category_update_path')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" class="form-control" name="id" value="{{old('category_id',isset($category) ? @$category['category_id'] : null)}}">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên danh mục</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Tên danh mục" name="name" value="{{old('category_name',isset($category) ? @$category['category_name'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thuộc danh mục</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="parent">
                                <option value="0">Thuộc danh mục</option>
                                @foreach($categories as $key=>$cate)
                                    <option value="{{$key}}" @if($key==$category['category_id']) selected="selected" @endif>{{$cate}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vị trí</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="position">
                                <option value="0" @if($category['category_position']==0) selected="selected" @endif>Top</option>
                                <option value="1" @if($category['category_position']==1) selected="selected" @endif>Bottom</option>
                                <option value="2" @if($category['category_position']==2) selected="selected" @endif>Right</option>
                                <option value="3" @if($category['category_position']==3) selected="selected" @endif>Left</option>
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thứ tự</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control"  placeholder="Thứ tự" name="order" value="{{old('category_order',isset($categories) ? @$categories['category_order'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-md-2 control-label">Trạng thái</label>
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input value="1" type="checkbox" name="status" @if(@$category['category_status']==1) checked="checked" @endif>
                                    Hiện
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SEO</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="seo">{{ old('seo',isset($category) ? @$category['category_seo'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keywords</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="keywords">{{ old('keywords',isset($category) ? @$category['category_keywords'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="description">{{ old('description',isset($category) ? @$category['category_description'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.form-body -->
                <div class="form-footer">
                    <div class="col-sm-offset-2">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div><!-- /.form-footer -->
            </form>
        </div>
    </div>
@endsection
