@extends('backend.index')
@section('title')
    Thêm danh mục
@endsection
@section('content')
    <h3><i class="fa fa-folder-open"></i> Thêm danh mục</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" action="{{route('category_store_path')}}" method="post">
                {{csrf_field()}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên danh mục</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Tên danh mục" name="name">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thuộc danh mục</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="parent">
                                <option value="0">Thuộc danh mục</option>
                                @foreach($categorys as $key=>$cate)
                                    <option value="{{$key}}">{{$cate}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vị trí</label>
                        <div class="col-sm-2">
                            <select class="form-control" name="position">
                                <option value="0">Top</option>
                                <option value="1">Bottom</option>
                                <option value="2">Right</option>
                                <option value="3">Left</option>
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thứ tự</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control"  placeholder="Thứ tự" name="order">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Trạng thái</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label>
                                    <input value="1" type="checkbox" name="status">
                                    Hiện
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SEO</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="seo"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keywords</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="keywords"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="description"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.form-body -->
                <div class="form-footer">
                    <div class="col-sm-offset-2">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div><!-- /.form-footer -->
            </form>
        </div>
    </div>
@endsection
