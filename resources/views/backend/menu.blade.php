<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <?php
        $menu_name = strtolower($structure_info['name']);
        $parent_menu_name = strtolower($structure_info['method']);
        $menu_permission_parent = array();
        foreach($menu_list as $menu)
        {
            if($menu['status'] == 1)
            {
                $menu_permission[] = $menu['module_id'];
            }
            if(!in_array($menu['action_menu'], $menu_permission_parent) && ($menu['status'] == 1))
            {
                $menu_permission_parent[] = $menu['action_menu'];
            }
        }
        ?>
        <ul class="nav" id="side-menu">
            @foreach($menu_action as $key => $menu)
                <?php
                $menu_flag = '';
                $sub = $menu_action[$key];
                if(in_array($key, $menu_permission_parent)):
                ?>
                <li class="sub-menu">
                    <a href="javascript:;" class="<?php if($parent_menu_name == $key){ echo 'active'; $menu_flag = 1; } ?>">
                        <?php echo $menu[0]['icon']?> {{$key }}@if(!($menu[0]['show_menu'])=='')<span class="fa arrow">@endif</span>
                    </a>
                    <ul class="nav nav-second-level">
                        @foreach($sub as $sub_item)
                            <?php
                            if((in_array($sub_item['module_id'], $menu_permission))):
                            $main_path = strtolower($sub_item['name']);
                            foreach($menu_list as $menu)
                            {
                                if($main_path == strtolower($menu['module_name']))
                                {
                                    $temp_path = explode(",", $menu['action']);
                                    $sub_path = $temp_path[0];
                                }
                            }
                            ?>
                            <li class="<?php echo (($menu_name == strtolower($sub_item['name'])) && ($menu_flag == 1)) ? 'active' : ''; ?>">
                                <?php
                                $path = $main_path . '_' .$sub_path.'_path';
                                ?>
                                <a href="{{ route($path) }}"><?php echo ucfirst($sub_item['show_menu']);?></a>
                            </li>
                            <?php
                            endif;
                            ?>
                        @endforeach
                    </ul>
                </li>
                <?php endif; ?>
            @endforeach
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>