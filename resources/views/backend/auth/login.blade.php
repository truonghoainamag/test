<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('images/favico.png')}}"/>
    <title>Đăng nhập hệ thống</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/jasny-bootstrap-fileinput.min.css')}}" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <![endif]-->

</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <form action="{{route('admin_run_path')}}" method="POST">
                    {{csrf_field()}}
                    <div class="col-lg-offset-4 col-lg-4">
                        <div class="form-group">
                            <h1>Đăng nhập hệ thống</h1>
                        </div>
                        <div class="form-group has-success">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control" name="username" placeholder="Tên đăng nhập" required autofocus >
                            </div>
                        </div>
                        <div class="form-group has-success">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <input type="password" class="form-control" name="password" placeholder="Mật khẩu" required autofocus >
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"> Ghi nhớ
                                </label>
                            </div>
                        </div>
                        <button class="btn btn-lg btn-success btn-block" type="submit"> Đăng nhập</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<!-- jQuery -->
<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
</body>
</body>

</html>
