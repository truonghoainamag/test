
<?php
use App\Model\Category;
?>
@extends('backend.index')
@section('title')
    Module
@endsection
@section('content')
    <h3><i class="fa fa-desktop"></i> Module</h3>
    <h4><a href="{{route('modules_create_path')}}" class="btn btn-success">Thêm mới</a></h4>
    <div class="table-responsive" style="margin-top: -1px;">
        <table class="table table-striped table-primary table-bordered">
            <thead>
            <tr>
                <th class="text-center border-right" style="width: 1%;">No.</th>
                <th>Name</th>
                <th>Action</th>
                <th>Show Menu</th>
                <th>Action Menu</th>
                <th>Icon</th>
                <th>Order</th>
                <th>Status</th>
                <th>Created Date</th>
                <th class="text-center" style="width: 12%;">Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($list_modules))
                @if(count($list_modules) > 0)
                    <?php $no=1; ?>
                    @foreach($list_modules as $modules)
                        @if($modules['status']==1)
                            <?php $status='Hiện'; ?>
                        @else
                            <?php $status='Ẩn'; ?>
                        @endif
                        <tr>
                            <td class="text-center border-right">{{$no}}</td>
                            <td>
                                <span>{{$modules['name']}}</span>
                            </td>
                            <td>{{$modules['action']}}</td>
                            <td>{{$modules['show_menu']}}</td>
                            <td>{{$modules['action_menu']}}</td>
                            <td>{{$modules['icon']}}</td>
                            <td class="text-center">{{$modules['order']}}</td>
                            <td class="text-center">{{$status}}</td>
                            <td>{{$modules['created_date']}}</td>
                            <td class="text-center">
                                <a href="{{route('modules_edit_path',$modules['module_id'])}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('modules_delete_path',$modules['module_id'])}}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php $no++; ?>
                    @endforeach
                @else
                    <tr>
                        <td colspan="9" class="text-center">Danh sách rổng</td>
                    </tr>
                @endif
            @endif
            </tbody>
        </table>
    </div><!-- /.table-responsive -->
    @if(isset($list_modules))
        @if(count($list_modules)>0)
            <nav class="pull-right">
                <ul class="pagination pagination_trang">
                    @if($list_modules->currentPage()!=1)
                        <li>
                            <a href="{{$list_modules->url($list_modules->currentPage()-1)}}">
                                Trở lại
                            </a>
                        </li>
                    @endif
                    @for($i=1;$i<=$list_modules->lastPage();$i++)
                        <li class="{{($list_modules->currentPage()==$i) ? 'active':''}}">
                            <a href="{{$list_modules->url($i)}}">{{$i}}</a>
                        </li>
                    @endfor
                    @if($list_modules->currentPage()!=$list_modules->lastPage())
                        <li>
                            <a href="{{$list_modules->url($list_modules->currentPage()+1)}}">
                                Tiếp tục
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        @endif
    @endif
@endsection