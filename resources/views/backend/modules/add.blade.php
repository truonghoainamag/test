@extends('backend.index')
@section('title')
    Thêm Module
@endsection
@section('content')
    <h3><i class="fa fa-desktop"></i> Thêm module</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal mt-10" method="POST">
                {{csrf_field()}}
                <input type="hidden" class="form-control" name="id" value="{{old('id',isset($list_modules) ? @$list_modules['module_id'] : null)}}">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name Modules</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Name Modules" name="name" value="{{old('name',isset($list_modules) ? @$list_modules['name'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Action</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Action" name="action" value="{{old('action',isset($list_modules) ?@$list_modules['action'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Show Menu</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Show Menu" name="show_menu" value="{{old('show_menu',isset($list_modules) ? @$list_modules['show_menu'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Action Menu</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Action Menu" name="action_menu" value="{{old('action_menu',isset($list_modules) ? @$list_modules['action_menu'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Icon</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Icon" name="icon" value="{{old('icon',isset($list_modules) ? @$list_modules['icon'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Order</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Icon" name="order" value="{{old('order',isset($list_modules) ? @$list_modules['order'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Trạng thái</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label for="rememberme2">
                                    <input id="rememberme2" value="1" type="checkbox" name="status" @if(@$list_modules['status']==1) checked="checked" @endif>
                                    Hiện
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.form-body -->
                <div class="form-footer">
                    <div class="col-sm-offset-3">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div><!-- /.form-footer -->
            </form>
        </div>
    </div>
@endsection
