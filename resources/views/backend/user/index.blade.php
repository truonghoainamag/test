<?php
use App\Model\Category;
?>
@extends('backend.index')
@section('title')
    Users
@endsection
@section('content')
    <h3><i class="fa fa-user"></i> Users</h3>
    <h4><a href="{{route('users_create_path')}}" class="btn btn-success">Thêm mới</a></h4>
    <div class="table-responsive">
        <table class="table table-striped table-primary table-bordered">
            <thead>
            <tr>
                <th class="text-center border-right" style="width: 1%;">No.</th>
                <th>User Name</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Group</th>
                <th>Status</th>
                <th class="text-center" style="width: 12%;">Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($list_uesers) && count($list_uesers) > 0)
                <?php $no=1; ?>
                @foreach($list_uesers as $uesers)
                    @if($uesers['status']==1)
                        <?php $status='Hiện'; ?>
                    @else
                        <?php $status='Ẩn'; ?>
                    @endif
                    <tr>
                        <td class="text-center border-right">{{$no}}</td>
                        <td>{{$uesers['username']}}</td>
                        <td>{{$uesers['fullname']}}</td>
                        <td> {{$uesers['email'] }}</td>
                        <td>{{$uesers['group_id']}}</td>
                        <td>{{$uesers['status']}}</td>
                        <td class="text-center">
                            <a href="{{route('user_edit_path',$uesers['user_id'])}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="{{route('user_destroy_path',$uesers['user_id'])}}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">Danh sách rổng</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    @if(isset($list_uesers))
        @if(count($list_uesers)>0)
            <nav class="pull-right">
                <ul class="pagination pagination_trang">
                    @if($list_uesers->currentPage()!=1)
                        <li>
                            <a href="{{$list_uesers->url($list_uesers->currentPage()-1)}}">
                                Trở lại
                            </a>
                        </li>
                    @endif
                    @for($i=1;$i<=$list_uesers->lastPage();$i++)
                        <li class="{{($list_uesers->currentPage()==$i) ? 'active':''}}">
                            <a href="{{$list_uesers->url($i)}}">{{$i}}</a>
                        </li>
                    @endfor
                    @if($list_uesers->currentPage()!=$list_uesers->lastPage())
                        <li>
                            <a href="{{$list_uesers->url($list_uesers->currentPage()+1)}}">
                                Tiếp tục
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        @endif
    @endif
@endsection