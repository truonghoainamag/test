<?php
use App\Model\Category;
?>
@extends('backend.index')
@section('title')
    Bài viết
@endsection
@section('content')
    <h3><i class="fa fa-folder-open"></i> Bài viết</h3>
    <h4><a href="{{route('article_create_path')}}" class="btn btn-success">Thêm mới</a></h4>
    <div class="table-responsive">
        <table class="table table-striped table-primary table-bordered thn_table">
            <thead>
            <tr>
                <th class="text-center border-right" style="width: 1%;">No.</th>
                <th>Name</th>
                <th class="text-center">Category</th>
                <th class="text-center">Image</th>
                <th class="text-center">Status</th>
                <th class="text-center">View</th>
                <th class="text-center">Created date</th>
                <th class="text-center" style="width: 12%;">Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($article) && count($article) > 0)
                <?php $no=1; ?>
                @foreach($article as $arti)
                    @if($arti['article_status']==1)
                        <?php $status='Hiện'; ?>
                    @else
                        <?php $status='Ẩn'; ?>
                    @endif
                    <tr>
                        <td class="text-center border-right">{{$no}}</td>
                        <td>
                            <span>{{substr($arti['article_name'],0,50)}}.....</span>
                        </td>
                        <td>
                            <?php $cate_id=Category::getListId($arti['article_category']); ?>
                            <span>
                                @if(count($cate_id) > 0 )
                                    {{$cate_id['category_name']}}
                                @endif
                            </span>
                        </td>
                        <td class="text-center">
                            @if(empty($arti['article_image']))
                            <img src="{{asset('images/article/dorimon.png')}}" alt="Hình đại diện" class="img-responsive img-thumbnail" width="50px;"/>
                            @else
                                <img src="{{asset('images/article').'/'.$arti['article_image']}}" alt="Hình đại diện" class="img-responsive img-thumbnail" width="150px;"/>
                            @endif
                        </td>
                        <td class="text-center"> {{ $status }}</td>
                        <td class="text-center">{{$arti['article_view']}}</td>
                        <td class="text-center">{{$arti['article_created_at']}}</td>
                        <td class="text-center">
                            <a href="{{route('article_edit_path',$arti['article_id'])}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="{{route('article_destroy_path',$arti['article_id'])}}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <?php $no++; ?>
                @endforeach
            @else
                <tr>
                    <td colspan="8" class="text-center">Danh sách rổng</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
    @if(isset($article))
        @if(count($article)>0)
            <nav class="pull-right">
                <ul class="pagination pagination_trang">
                    @if($article->currentPage()!=1)
                        <li>
                            <a href="{{$article->url($article->currentPage()-1)}}">
                                Trở lại
                            </a>
                        </li>
                    @endif
                    @for($i=1;$i<=$article->lastPage();$i++)
                        <li class="{{($article->currentPage()==$i) ? 'active':''}}">
                            <a href="{{$article->url($i)}}">{{$i}}</a>
                        </li>
                    @endfor
                    @if($article->currentPage()!=$article->lastPage())
                        <li>
                            <a href="{{$article->url($article->currentPage()+1)}}">
                                Tiếp tục
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        @endif
    @endif
@endsection