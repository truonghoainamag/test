@extends('backend.index')
@section('title')
    Sửa bài viết
@endsection
@section('content')
    <h3><i class="fa fa-folder-open"></i> Sửa bài viết</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" action="{{route('article_update_path',$article[0]['article_id'])}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên bài viết</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" rows="2" name="name">{{ old('name',isset($article) ? $article[0]['article_name'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ảnh đại diện</label>
                        <div class="col-sm-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 300px; height: 200px;">
                                    @if(empty($article[0]['article_image']))
                                    <img src="{{asset('images/article/dorimon.png')}}">
                                    @else
                                    <img src="{{asset('images/article').'/'.$article[0]['article_image']}}"/>
                                    @endif
                                </div>
                                <div>
                                    <span class="btn btn-info btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="images">
                                        <input type="hidden" name="photo" value="{{$article[0]['article_image']}}">
                                    </span>
                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thuộc danh mục</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="category">
                                <option value="0">Thuộc danh mục</option>
                                @foreach($category as $cate)
                                    <option value="{{$cate['category_id']}}" @if($cate['category_id']==$article[0]['article_category']) selected="selected" @endif>{{$cate['category_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tiêu bài viết</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="title">{{ old('title',isset($article) ? $article[0]['article_title'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nội dung bài viết</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="4" name="content">{{ old('content',isset($article) ? $article[0]['article_content'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Video</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control"  placeholder="video" name="article_video" value="{{ old('article_video',isset($article) ? @$article[0]['article_video'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thứ tự</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control"  placeholder="Thứ tự" name="order" value="{{ old('order',isset($article) ? @$article[0]['article_order'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Trạng thái</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label>
                                    <input value="1" type="checkbox" name="status" @if($article[0]['article_status']==1) checked="checked" @endif>
                                    Hiện
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tin nóng</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label>
                                    <input value="1" type="checkbox" name="hot" @if($article[0]['article_hot']==1) checked="checked" @endif>
                                    Hot
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SEO</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="seo">{{ old('seo',isset($article) ? @$article[0]['article_seo'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keywords</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="keywords">{{ old('keywords',isset($article) ? @$article[0]['article_keywords'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="description">{{ old('description',isset($article) ? @$article[0]['article_description'] : null)}}</textarea>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.form-body -->
                <div class="form-footer">
                    <div class="col-sm-offset-2">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div><!-- /.form-footer -->
            </form>
        </div>
    </div>
@endsection
