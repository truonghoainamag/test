@extends('backend.index')
@section('title')
    Thêm bài viết
@endsection
@section('content')
    <h3><i class="fa fa-folder-open"></i> Thêm bài viết</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal" action="{{route('article_store_path')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tên bài viết</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" rows="2" name="name"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Ảnh đại diện</label>
                        <div class="col-sm-6">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
                                <div>
                                    <span class="btn btn-info btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="images"></span>
                                    <a href="#" class="btn btn-danger fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thuộc danh mục</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="category">
                                <option value="0">Thuộc danh mục</option>
                                @foreach($category as $cate)
                                    <option value="{{$cate['category_id']}}">{{$cate['category_name']}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tiêu bài viết</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="title"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nội dung bài viết</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="4" name="content"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Video</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control"  placeholder="Video" name="article_video">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Thứ tự</label>
                        <div class="col-sm-2">
                            <input type="text" class="form-control"  placeholder="Thứ tự" name="order">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Trạng thái</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label>
                                    <input value="1" type="checkbox" name="status">
                                    Hiện
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tin nóng</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label>
                                    <input value="1" type="checkbox" name="hot">
                                    Hot
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">SEO</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="seo"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keywords</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="keywords"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="3" name="description"></textarea>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.form-body -->
                <div class="form-footer">
                    <div class="col-sm-offset-2">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div><!-- /.form-footer -->
            </form>
        </div>
    </div>
@endsection
