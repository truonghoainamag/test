<?php
use App\Model\Category;
?>
@extends('backend.index')
@section('title')
    Group
@endsection
@section('content')
    <h3><i class="fa fa-users"></i> Group</h3>
    <h4><a href="{{route('groups_create_path')}}" class="btn btn-success">Thêm mới</a></h4>
    <div class="table-responsive">
        <table class="table table-striped table-primary table-bordered">
            <thead>
            <tr>
                <th class="text-center border-right" style="width: 1%;">No.</th>
                <th>Name</th>
                <th>Status</th>
                <th>Created date</th>
                <th class="text-center" style="width: 12%;">Action</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($list_groups))
                @if(count($list_groups) > 0)
                    <?php $no=1; ?>
                    @foreach($list_groups as $group)
                        @if($group['status']==1)
                            <?php $status='Hiện'; ?>
                        @else
                            <?php $status='Ẩn'; ?>
                        @endif
                        <tr>
                            <td class="text-center border-right">{{$no}}</td>
                            <td>
                                <span>{{$group['name']}}</span>
                            </td>
                            <td>{{$status}}</td>
                            <td>{{$group['created_date']}}</td>
                            <td class="text-center">
                                <a href="{{route('groups_edit_path',$group['group_id'])}}" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                                <a href="{{route('groups_delete_path',$group['group_id'])}}" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" data-original-title="Delete"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        <?php $no++; ?>
                    @endforeach
                @else
                    <tr>
                        <td colspan="6" class="text-center">Danh sách rổng</td>
                    </tr>
                @endif
            @endif
            </tbody>
        </table>
    </div>
    @if(isset($list_groups))
        @if(count($list_groups)>0)
            <nav class="pull-right">
                <ul class="pagination pagination_trang">
                    @if($list_groups->currentPage()!=1)
                        <li>
                            <a href="{{$list_groups->url($list_groups->currentPage()-1)}}">
                                Trở lại
                            </a>
                        </li>
                    @endif
                    @for($i=1;$i<=$list_groups->lastPage();$i++)
                        <li class="{{($list_groups->currentPage()==$i) ? 'active':''}}">
                            <a href="{{$list_groups->url($i)}}">{{$i}}</a>
                        </li>
                    @endfor
                    @if($list_groups->currentPage()!=$list_groups->lastPage())
                        <li>
                            <a href="{{$list_groups->url($list_groups->currentPage()+1)}}">
                                Tiếp tục
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
        @endif
    @endif
@endsection