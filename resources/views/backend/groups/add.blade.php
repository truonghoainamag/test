@extends('backend.index')
@section('title')
    Thêm bài viết
@endsection
@section('content')
    <h3><i class="fa fa-users"></i>  Thêm groups</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal mt-10" method="POST">
                {{csrf_field()}}
                <input type="hidden" class="form-control" name="id" value="{{old('group_id',isset($list_groups) ? @$list_groups['group_id'] : null)}}">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Name Group</label>
                        <div class="col-sm-4">
                            <input type="text" class="form-control"  placeholder="Name Group" name="name" value="{{old('name',isset($list_groups) ? @$list_groups['name'] : null)}}">
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Trạng thái</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label for="rememberme2"><input id="rememberme2" value="1" type="checkbox" name="status" @if(@$list_groups['status']==1) checked="checked" @endif>
                                    Hiện
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.form-body -->
                <div class="form-footer">
                    <div class="col-sm-offset-3">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div><!-- /.form-footer -->
            </form>
        </div>
    </div>
@endsection
