<?php
use App\Model\Category;
?>
@extends('backend.index')
@section('title')
    Permissions
@endsection
@section('content')
    <h3><i class="fa fa-user-plus"></i> Permissions</h3>
    <form action="{{route('permissions_add_path')}}" class="form-horizontal" method="post">
        {{csrf_field()}}
        <div class="form-group">
            <label for="group" class="col-sm-1 control-label">Group</label>
            <div class="col-sm-2">
                <select class="form-control" name="group_id">
                    @if(isset($list_group))
                        @foreach($list_group as $group)
                            <option value="{{$group['group_id']}}">{{$group['name']}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-sm-9">
                <div class="pull-right">
                    <button type="submit" class="btn  btn-success">Cập nhật</button>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-primary table-bordered">
                <thead>
                <tr>
                    <th class="text-center border-right" style="width: 1%;">No.</th>
                    <th class="col-md-2">Modules</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(isset($list_module))
                    @if(count($list_module) > 0)
                        <?php $no=1; ?>
                        @foreach($list_module as $module)
                            <?php
                            $permission_action = array();
                            if(!empty($data['permission'][$module['module_id']])){
                                $list_permission=explode(',',$data['permission'][$module['module_id']]['action']);
                            }
                            $list_action= explode(',',$module['action']);
                            ?>
                            @if($module['status']==1)
                                <?php $status='Hiện'; ?>
                            @else
                                <?php $status='Ẩn'; ?>
                            @endif
                            <tr>
                                <td class="text-center border-right">{{$no}}</td>
                                <td>{{$module['name']}}</td>
                                <td>
                                    <div class="checkbox">
                                        @foreach($list_action as $action)
                                            <label>
                                                <input <?php echo in_array($action,$list_permission) ? 'checked="checked"' : ''; ?> type="checkbox" name="checkbox[{{$module['module_id']}}][{{$action}}]" value="1"> {{$action}}
                                            </label>
                                        @endforeach
                                    </div>
                                </td>
                            </tr>
                            <?php $no++; ?>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7" class="text-center">Danh sách rổng</td>
                        </tr>
                    @endif
                @endif
                </tbody>
            </table>
        </div>
    </form>
@endsection