@extends('backend.index')
@section('title')
    Thêm permissions
@endsection
@section('content')
    <h3><i class="fa fa-user-plus"></i>  Thêm mới</h3>
    <div class="panel panel-default">
        <div class="panel-body">
            <form class="form-horizontal mt-10" method="POST">
                {{csrf_field()}}
                <input type="hidden" class="form-control" name="id" value="{{old('id',isset($list_modules) ? @$list_modules['module_id'] : null)}}">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Groups</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="group">
                                @if(isset($list_group))
                                   @foreach($list_group as $group)
                                        <option value="{{$group['group_id']}}">{{$group['name']}}</option>
                                   @endforeach
                                @endif
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Modules</label>
                        <div class="col-sm-4">
                            <select class="form-control" name="module">
                                @if(isset($list_module))
                                    @foreach($list_module as $module)
                                        <option value="{{$module['module_id']}}">{{$module['name']}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Action Menu</label>
                        <div class="col-sm-4">
                            <select multiple class="form-control">
                                <option selected="selected">Index</option>
                                <option selected="selected">Add</option>
                                <option selected="selected">Edit</option>
                                <option selected="selected">Delete</option>
                                <option selected="selected">Index</option>
                                <option selected="selected">Add</option>
                                <option selected="selected">Edit</option>
                                <option selected="selected">Delete</option>
                            </select>
                        </div>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Trạng thái</label>
                        <div class="col-sm-7">
                            <div class="checkbox">
                                <label for="rememberme2"><input id="rememberme2" value="1" type="checkbox" name="status" @if(@$list_groups['status']==1) checked="checked" @endif>
                                    Hiện
                                </label>
                            </div>
                        </div>
                    </div><!-- /.form-group -->
                </div><!-- /.form-body -->
                <div class="form-footer">
                    <div class="col-sm-offset-3">
                        <button type="submit" class="btn btn-success">Thêm</button>
                    </div>
                </div><!-- /.form-footer -->
            </form>
        </div>
    </div>
@endsection
