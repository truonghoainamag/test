<header>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{route('front_index_path')}}">Trang chủ</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="{{route('front_about_path')}}">ABOUT</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height: 76px;">
              {{--<h1>Newsfee.net</h1>--}}
                <script type="text/javascript">
                    google_ad_client = "ca-pub-6373019903779555";
                    google_ad_slot = "1817237428";
                    google_ad_width = 728;
                    google_ad_height = 90;
                </script>
                <!-- googleadsense -->
                <script type="text/javascript"
                        src="//pagead2.googlesyndication.com/pagead/show_ads.js">
                </script>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand thn_navbar-brand" href="{{route('front_index_path')}}"><i class="fa fa-home fa-2x"></i></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse1" data-hover="dropdown"
                 data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
                <ul class="nav navbar-nav thn_menu_bottom">
                    <?php echo $html; ?>
                </ul>
                <form class="navbar-form navbar-right thn_form_search" role="search" action="{{route('frontend_search_path')}}" method="GET">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Tìm kiếm" name="search" id="srch-term" value="<?=@$search ?>">
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.navbar-collapse -->
        </div>
    </nav>
</header>
