<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <div class="panel panel-default">
        <div class="panel-heading">Hướng dẫn</div>
        <div class="panel-body">
            <ul class="list-unstyled">
                @foreach($category_left as $left)
                    <li><a href="{{route('frontend_category_path',[$left['category_id'],$left['category_seo']])}}">{{$left['category_name']}}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</div>