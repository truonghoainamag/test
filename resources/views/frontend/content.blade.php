<article>
    <div class="container-fluid">
        <div class="row">
            @include('frontend.content_left')
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 thn_content">
                <div class="panel panel-default">
                    <div class="panel-heading">BÀI MỚI NHẤT</div>
                </div>
                @if(isset($list_article) && count($list_article)>0)
                   @foreach($list_article as $article)
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <article>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <h3><a href="{{route('frontend_article_path',[$article['article_id'],$article['article_seo']])}}">{{$article['article_name']}}</a></h3>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thn_hr">
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <span><i class="fa fa-user"></i>  Trương Hoài Nam</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <span><i class="fa fa-clock-o"></i> {{date('d/m/Y',strtotime($article['article_created_at']))}}</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <span><i class="fa fa-bar-chart"></i> 500</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 thn_image">
                                        <a href=""><img src="{{asset('images/article/'.$article['article_image'])}}" class="img-responsive"></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 thn_article_content">
                                        <p>
                                            {{$article['article_title']}}
                                            <a href="{{route('frontend_article_path',[$article['article_id'],$article['article_seo']])}}">Đọc tiếp <i class="fa fa-angle-double-right"></i></a>
                                        </p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    @endforeach
                @endif
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    @if(isset($list_article))
                        @if(count($list_article)>0)
                            <nav class="text-center">
                                <ul class="pagination pagination_trang">
                                    @if($list_article->currentPage()!=1)
                                        <li>
                                            <a href="{{$list_article->url($list_article->currentPage()-1)}}">
                                                Trở lại
                                            </a>
                                        </li>
                                    @endif
                                    @for($i=1;$i<=$list_article->lastPage();$i++)
                                        <li class="{{($list_article->currentPage()==$i) ? 'active':''}}">
                                            <a href="{{$list_article->url($i)}}">{{$i}}</a>
                                        </li>
                                    @endfor
                                    @if($list_article->currentPage()!=$list_article->lastPage())
                                        <li>
                                            <a href="{{$list_article->url($list_article->currentPage()+1)}}">
                                                Tiếp tục
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        @endif
                    @endif
                </div>
            </div>
            @include('frontend.content_right')
        </div>
    </div>
</article>