<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="footer_top">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <ul class="list-unstyled">
                        <h4>BÀI MỚI NHẤT</h4>
                        @foreach($list_article_new as $article_new)
                            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <a href="{{route('frontend_article_path',[$article_new['article_id'],$article_new['article_seo']])}}">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 thn_col4">
                                        <img src="{{asset('images/article/'.$article_new['article_image'])}}" class="img-responsive" alt="image">
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 thn_col8">
                                        <p>{{$article_new['article_name']}}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <ul class="list-unstyled">
                        <h4>BÀI XEM NHIỀU NHẤT</h4>
                        @foreach($list_article_top as $article_top)
                            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <a href="{{route('frontend_article_path',[$article_top['article_id'],$article_top['article_seo']])}}">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 thn_col4">
                                        <img src="{{asset('images/article/'.$article_top['article_image'])}}" class="img-responsive" alt="image">
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 thn_col8">
                                        <p>{{$article_top['article_name']}}</p>
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <ul class="list-unstyled">
                        <h4>VIDEO MỚI NHẤT</h4>
                        @foreach($list_article_new as $article_new)
                            @if(!empty($article_new['article_video']))
                            <li class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <a href="{{route('frontend_article_path',[$article_new['article_id'],$article_new['article_seo']])}}">
                                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 thn_col4">
                                        <img src="{{asset('images/article/'.$article_new['article_image'])}}" class="img-responsive" alt="image">
                                    </div>
                                    <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 thn_col8">
                                        <p>{{$article_new['article_name']}}</p>
                                    </div>
                                </a>
                            </li>
                            @endif
                        @endforeach
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                    <ul class="list-unstyled">
                        <h4>THÔNG TIN LIÊN HỆ</h4>
                        <li>Email: truonghoainamag@gmail.com</li>
                        <li>Điện thoại: 0966034280</li>
                        <li>Skype:truonghoainamag@gmail.com</li>
                        <li>Facebook:truonghoainam</li>
                        <li>
                            <p>Giới thiệu: newsfee.net được lập vào đầu năm 2016 với mục đích chia sẽ những kinh nghiệm,
                                kỹ thuật về lập trình PHP.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thn_footer_line"></div>
            <div class="footer_bottom">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    Copyright © 2015 - 2020.All rights reserved. Edited by newsfee.net
                </div>
            </div>
        </div>
    </div>
</footer>
