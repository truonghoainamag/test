@extends('frontend.template')
@section('title') Chi tiết @endsection
@section('seo')<meta name="robots" content="index, follow">
    <meta name="description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}"/>
    <meta name="robots" content="index, follow">
    <link rel="canonical" href="{{Request::URL()}}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_keywords'],0,70),'p') : 'Học lập trình php, Học lập trình php online, Học php framework' }}" />
    <meta property="og:description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta property="og:url" content="{{Request::URL()}}" />
    <meta property="og:site_name" content="Hướng dẫn học lập trình php" />
    <meta property="article:publisher" content="https://www.facebook.com/groups/771798902929018/" />
    <meta property="article:tag" content="Hkboot.iso" />
    <meta property="article:tag" content="Hoàng Khi?n Boot" />
    <meta property="article:tag" content="USB Boot" />
    <meta property="article:section" content="Boot" />
    <meta property="article:published_time" content="2015-12-20T11:49:01+00:00" />
    <meta property="article:modified_time" content="2015-12-20T11:56:40+00:00" />
    <meta property="og:updated_time" content="2015-12-20T11:56:40+00:00" />
    <meta property="og:image" content="http://fpttelecom.edu.vn/wp-content/uploads/2015/12/menu-boot-hk.jpg" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="418" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta name="twitter:title" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta name="twitter:image" content="http://fpttelecom.edu.vn/wp-content/uploads/2015/12/menu-boot-hk.jpg" />
@endsection
@section('content')
    <article>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9 thn_font_size">
                    @if(isset($list_article_detail) && count($list_article_detail)>0)
                        @foreach($list_article_detail as $article)
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <article>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <h3><a href="{{route('frontend_article_path',[$article['article_id'],$article['article_name']])}}">{{$article['article_name']}}</a></h3>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thn_hr">
                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                <span><i class="fa fa-user"></i>  Trương Hoài Nam</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                <span><i class="fa fa-clock-o"></i> {{date('d/m/Y',strtotime($article['article_created_at']))}}</span>
                                            </div>
                                            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                                <span><i class="fa fa-bar-chart"></i> 500</span>
                                            </div>
                                        </div>
                                        @if(!empty($article['article_video']))
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <iframe width="100%;" height="400px;" src="{{$article['article_video']}}" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                        @endif
                                        <div class="clearfix"></div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                            <p>
                                                <?php echo $article['article_content']?>
                                            </p>
                                        </div>
                                    </article>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-body">
						<div class="fb-share-button" data-href="{{Request::URL()}}" data-layout="button_count" data-mobile-iframe="true"></div>
                            <div class="fb-comments fb-1" data-href="{{Request::URL()}}" data-num-posts="10"
                                 data-width="100%" data-colorscheme="light"></div> 
                        </div>
                    </div>
                </div>
                @include('frontend.content_left')
                @include('frontend.content_right')
            </div>
        </div>
    </article>
@endsection
