<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
    <div class="panel panel-default">
        <div class="panel-heading">TÀI NGUYÊN</div>
        <div class="panel-body">
            <ul class="list-unstyled">
                @foreach($category_right as $right)
                <li><a href="{{route('frontend_category_path',[$right['category_id'],$right['category_seo']])}}">{{$right['category_name']}}</a></li>
                @endforeach
            </ul>
            </ul>
        </div>
    </div>
</div>