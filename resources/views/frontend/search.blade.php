
@extends('frontend.template')
@section('title') Kết quả tìm kiếm @endsection
@section('seo')<meta name="robots" content="index, follow">
    <meta name="robots" content="index, follow">
    <meta name="description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}"/>
    <meta name="robots" content="index, follow">
    <link rel="canonical" href="{{Request::URL()}}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_keywords'],0,70),'p') : 'Học lập trình php, Học lập trình php online, Học php framework' }}" />
    <meta property="og:description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta property="og:url" content="{{Request::URL()}}" />
    <meta property="og:site_name" content="Hướng dẫn học lập trình php" />
    <meta property="article:publisher" content="https://www.facebook.com/groups/771798902929018/" />
    <meta property="article:tag" content="Hkboot.iso" />
    <meta property="article:tag" content="Hoàng Khi?n Boot" />
    <meta property="article:tag" content="USB Boot" />
    <meta property="article:section" content="Boot" />
    <meta property="article:published_time" content="2015-12-20T11:49:01+00:00" />
    <meta property="article:modified_time" content="2015-12-20T11:56:40+00:00" />
    <meta property="og:updated_time" content="2015-12-20T11:56:40+00:00" />
    <meta property="og:image" content="http://fpttelecom.edu.vn/wp-content/uploads/2015/12/menu-boot-hk.jpg" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="418" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta name="twitter:title" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta name="twitter:image" content="http://fpttelecom.edu.vn/wp-content/uploads/2015/12/menu-boot-hk.jpg" />
@endsection
@section('content')
<article>
    <div class="container-fluid">
        <div class="row">
            @include('frontend.content_left')
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 thn_content">
                <div class="panel panel-default">
                    <div class="panel-heading">KẾT QUẢ TÌM KIẾM</div>
                </div>
                @if(isset($list_search) && count($list_search)>0)
                    @foreach($list_search as $article)
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <article>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                        <h3><a href="{{route('frontend_article_path',[$article['article_id'],$article['article_seo']])}}">{{$article['article_name']}}</a></h3>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 thn_hr">
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <span><i class="fa fa-user"></i>  Trương Hoài Nam</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <span><i class="fa fa-clock-o"></i> {{date('d/m/Y',strtotime($article['article_created_at']))}}</span>
                                        </div>
                                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                                            <span><i class="fa fa-bar-chart"></i> 500</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 thn_image">
                                        <a href=""><img src="{{asset('images/article/'.$article['article_image'])}}" class="img-responsive"></a>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 thn_article_content">
                                        <p>
                                            {{$article['article_title']}}
                                            <a href="{{route('frontend_article_path',[$article['article_id'],$article['article_seo']])}}">Đọc tiếp <i class="fa fa-angle-double-right"></i></a>
                                        </p>
                                    </div>
                                </article>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3>Dữ liệu rỗng</h3>
                        </div>
                    </div>
                @endif
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                    @if(isset($list_search))
                        @if(count($list_search)>0)
                            <nav class="text-center">
                                <ul class="pagination pagination_trang">
                                    @if($list_search->currentPage()!=1)
                                        <li>
                                            <a href="{{$list_search->url(isset($search) ? '?search='.$search : ''.$list_search->currentPage()-1)}}">
                                                Trở lại
                                            </a>
                                        </li>
                                    @endif
                                    @for($i=1;$i<=$list_search->lastPage();$i++)
                                        <li class="{{($list_search->currentPage()==$i) ? 'active':''}}">
                                            <a href="{{$list_search->url($i)}}">{{$i}}</a>
                                        </li>
                                    @endfor
                                    @if($list_search->currentPage()!=$list_search->lastPage())
                                        <li>
                                            <a href="{{$list_search->url(isset($search) ? '?search='.$search : ''.$list_search->currentPage()+1)}}">
                                                Tiếp tục
                                            </a>
                                        </li>
                                    @endif
                                </ul>
                            </nav>
                        @endif
                    @endif
                </div>
            </div>
            @include('frontend.content_right')
        </div>
    </div>
</article>
@endsection