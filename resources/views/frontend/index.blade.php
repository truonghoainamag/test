@extends('frontend.template')
@section('title')Học lập trình php online @endsection
@section('seo')<meta name="robots" content="index, follow">
    <meta name="description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}"/>
    <meta name="robots" content="index, follow">
    <link rel="canonical" href="{{Request::URL()}}" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_keywords'],0,70),'p') : 'Học lập trình php, Học lập trình php online, Học php framework' }}" />
    <meta property="og:description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta property="og:url" content="{{Request::URL()}}" />
    <meta property="og:site_name" content="Hướng dẫn học lập trình php" />
    <meta property="article:publisher" content="https://www.facebook.com/groups/771798902929018/" />
    <meta property="article:tag" content="Hkboot.iso" />
    <meta property="article:tag" content="Hoàng Khi?n Boot" />
    <meta property="article:tag" content="USB Boot" />
    <meta property="article:section" content="Boot" />
    <meta property="article:published_time" content="2015-12-20T11:49:01+00:00" />
    <meta property="article:modified_time" content="2015-12-20T11:56:40+00:00" />
    <meta property="og:updated_time" content="2015-12-20T11:56:40+00:00" />
    <meta property="og:image" content="{{asset('images/favicon.ico')}}" />
    <meta property="og:image:width" content="800" />
    <meta property="og:image:height" content="418" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:description" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta name="twitter:title" content="{{isset($list_article_detail) ? strip_tags(substr($list_article_detail[0]['article_description'],0,160),'p') : 'Học lập trình php online một cách hiệu quả, serial hướng dẫn php từ căn bản đến năng cao. Học php framework'}}" />
    <meta name="twitter:image" content="{{asset('images/favicon.ico')}}" />
@endsection
@section('content')
    @include('frontend.content')
@endsection
