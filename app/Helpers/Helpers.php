<?php
namespace App\Helpers;
use App\Model\Category;
use Illuminate\Support\Facades\Route;
class Helpers
{
    public $recursive;
    public $html;

    public function __construct()
    {
        $this->recursive = NULL;
        $this->html;
    }

    public function backend_menu($parentId = 0, $data = null, $string = '')
    {
        if (isset($data) && count($data) > 0) {
            foreach ($data as $values) {
                if ($values['category_parent'] == $parentId) {
                    $this->recursive[$values['category_id']] = $string . $values['category_name'];
                    $this->backend_menu($values['category_id'], $data);
                }
            }
        }
        return $this->recursive;
    }

    public function utf8convert($str)
    {
        if (!$str) return false;
        $utf8 = array(
            'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
            'd' => 'đ|Đ',
            'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
            'i' => 'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
            'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
            'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
            'y' => 'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        foreach ($utf8 as $ascii => $uni) {
            $str = preg_replace("/($uni)/i", $ascii, $str);
            $str=strtolower(str_replace(':','',str_replace(' ','-',$str)));
        }
        return $str;
    }
    public function frontend_menu(){
        $list_category=Category::getListAll(array('category_parent'=>0));
        foreach ($list_category as $category){
            $list_category_parent=Category::getListAll(array('category_parent'=>$category['category_id']));
            if(isset($list_category_parent) && count($list_category_parent)>0){
                $dropdown='dropdown';
                $links='';
                $caret='caret';
            }else{
                $dropdown='';
                $caret='';
                $links=route('frontend_category_path',[$category['category_id'],$category['category_seo']]);;
            }
            $this->html.='<li class="'.$dropdown.'">';
            $this->html.='<a href="'.$links.'">'.$category['category_name'].'<span class="'.$caret.'"></span></a>';
            $this->html.='<ul class="dropdown-menu">';
            foreach ($list_category_parent as $list_categorys) {
                $list_category_parent1 = Category::getListAll(array('category_parent' => $list_categorys['category_id']));
                if(isset($list_category_parent1) && count($list_category_parent1)>0){
                    $dropdown='dropdown';
                    $caret='caret';
                    $links='';
                }else{
                    $dropdown='';
                    $caret='';
                    $links=route('frontend_category_path',[$list_categorys['category_id'],$list_categorys['category_seo']]);
                }
                $this->html .= '<li class="'.$dropdown.'">';
                $this->html .= '<a href="'.$links.'">'.$list_categorys['category_name'].'<span class="'.$caret.'"></span></a>';
                $this->html .= '<ul class="dropdown-menu dropdownhover-right">';
                foreach ($list_category_parent1 as $list_categorys1) {
                    $links=route('frontend_category_path',[$list_categorys1['category_id'],$list_categorys1['category_seo']]);
                    $this->html .= ' <li><a href="'.$links.'">'.$list_categorys1['category_name'].'</a></li>';
                }
                $this->html .= '</ul>';
                $this->html .= ' </li>';
            }
            $this->html .= '</ul>';
            $this->html.='</li>';
        }
        return $this->html;
    }

}
?>