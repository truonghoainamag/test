<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Session;
use App\Model\Permissions;
class Authenticate
{
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = explode("_",str_replace("_path", "",$request->route()->getName()));
        dd($request->route());
        $module = $route[0];
        $action = $route[1];
        $isAuth = $request->header('Authenticate');
        if($isAuth == 'none'){
            return $next($request);
        } else{
            $user = Session::get('user');
            if(!isset($user)){
                return redirect()->guest('admin/login');
            } else{
                $check =Permissions::checkPermission($user, $module, $action);
                if(empty($check)){
                    abort(404);
                }
            }
        }
        return $next($request);
    }
}
