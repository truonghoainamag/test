<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    /*Start router admin*/
    Route::get('/',['as'=>'front_index_path','uses'=>'Frontend\IndexController@index']);
    Route::get('about',['as'=>'front_about_path','uses'=>'Frontend\AboutController@index']);
    Route::get('bai-viet/{id}/{name}',['as'=>'frontend_article_path','uses'=>'Frontend\ArticleController@index']);
    Route::get('danh-muc/{id}/{name}',['as'=>'frontend_category_path','uses'=>'Frontend\CategoryController@index']);
    Route::get('tim-kiem',['as'=>'frontend_search_path','uses'=>'Frontend\ArticleController@search']);
    /*Start router admin*/
    Route::get('admin/',['as'=>'home_index_path','uses'=>'Backend\IndexController@index']);
    Route::get('admin/login',['as'=>'login_index_path','uses'=>'Auth\LoginController@login']);
    Route::get('admin/logout',['as'=>'logout_index_path','uses'=>'Auth\LoginController@logout']);
    Route::post('admin/run', ['as' => 'admin_run_path', 'uses' => 'Auth\LoginController@run']);
    Route::get('admin/user',['as'=>'user_index_path','uses'=>'Backend\UsersController@index']);
    Route::get('admin/user/create',['as'=>'users_create_path','uses'=>'Backend\UsersController@create']);
    Route::get('admin/user/edit/{id}',['as'=>'user_edit_path','uses'=>'Backend\UsersController@edit']);
    Route::get('admin/user/destroy/{id}',['as'=>'user_destroy_path','uses'=>'Backend\UsersController@destroy']);

    Route::get('admin/group',['as'=>'group_index_path','uses'=>'Backend\GroupsController@index']);
    Route::get('admin/module',['as'=>'module_index_path','uses'=>'Backend\ModulesController@index']);
    Route::get('admin/permissions',['as'=>'permissions_index_path','uses'=>'Backend\PermissionsController@index']);
    /*End router admin*/

    /*Start router category*/
    Route::get('admin/category',['as'=>'category_index_path','uses'=>'Backend\CategoryController@index']);
    Route::get('admin/category/create',['as'=>'category_create_path','uses'=>'Backend\CategoryController@create']);
    Route::post('admin/category/store',['as'=>'category_store_path','uses'=>'Backend\CategoryController@store']);
    Route::get('admin/category/edit/{id}',['as'=>'category_edit_path','uses'=>'Backend\CategoryController@edit']);
    Route::post('admin/category/update',['as'=>'category_update_path','uses'=>'Backend\CategoryController@update']);
    Route::get('admin/category/destroy/{id}',['as'=>'category_destroy_path','uses'=>'Backend\CategoryController@destroy']);
    /*End router category*/
    /*Start router groups*/
    Route::get('admin/article',['as'=>'article_index_path','uses'=>'Backend\ArticleController@index']);
    Route::get('admin/article/create',['as'=>'article_create_path','uses'=>'Backend\ArticleController@create']);
    Route::post('admin/article/store',['as'=>'article_store_path','uses'=>'Backend\ArticleController@store']);
    Route::get('admin/article/edit/{id}',['as'=>'article_edit_path','uses'=>'Backend\ArticleController@edit']);
    Route::post('admin/article/update/{id}',['as'=>'article_update_path','uses'=>'Backend\ArticleController@update']);
    Route::get('admin/article/destroy/{id}',['as'=>'article_destroy_path','uses'=>'Backend\ArticleController@destroy']);
    /*End router category*/
    /*Start router groups*/
    Route::get('admin/groups',['as'=>'groups_index_path','uses'=>'Backend\GroupsController@index']);
    Route::any('admin/groups/add',['as'=>'groups_create_path','uses'=>'Backend\GroupsController@create']);
    Route::any('admin/groups/edit/{id}',['as'=>'groups_edit_path','uses'=>'Backend\GroupsController@edit']);
    Route::get('admin/groups/delete/{id}',['as'=>'groups_delete_path','uses'=>'Backend\GroupsController@delete']);
    /*End router groups*/
    /*Start router modules*/
    Route::get('admin/modules',['as'=>'modules_index_path','uses'=>'Backend\ModulesController@index']);
    Route::any('admin/modules/add',['as'=>'modules_create_path','uses'=>'Backend\ModulesController@create']);
    Route::any('admin/modules/edit/{id}',['as'=>'modules_edit_path','uses'=>'Backend\ModulesController@edit']);
    Route::get('admin/modules/delete/{id}',['as'=>'modules_delete_path','uses'=>'Backend\ModulesController@delete']);
    /*End router modules*/
    /*Start router permissions*/
    Route::get('admin/permissions',['as'=>'permissions_index_path','uses'=>'Backend\PermissionsController@index']);
    Route::any('admin/permissions/add',['as'=>'permissions_add_path','uses'=>'Backend\PermissionsController@add']);
    Route::any('admin/permissions/edit/{id}',['as'=>'permissions_edit_path','uses'=>'Backend\PermissionsController@edit']);
    Route::get('admin/permissions/delete/{id}',['as'=>'permissions_delete_path','uses'=>'Backend\PermissionsController@delete']);
    /*End router modules*/
    /*End router admin*/
});
