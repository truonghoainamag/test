<?php

namespace App\Http\Controllers\Backend;

use App\Model\Groups;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class GroupsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->data = $this->setupLayout();
    }
    public function index(){
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "home",
            "method" => "dashboard"
        );
        $list_groups=Groups::getList();
        return view('backend.groups.index',compact('list_groups','data','structure_info'));
    }
    public function create(Request $request){
        if($_SERVER['REQUEST_METHOD']=='GET'){
            return view('backend.groups.add');
        }else{
            if($request->input('_token')){
                $insert=Groups::insert($request->input());
                if($insert){
                    return redirect()->route('groups_index_path');
                }
                else{
                    return redirect()->route('groups_add_path');
                }
            }else{

            }

        }
    }
    public function edit(Request $request,$id){
        if($_SERVER['REQUEST_METHOD']=='GET'){
            $list_groups=Groups::getListId($id);
            return view('backend.groups.add',compact('list_groups'));
        }else{
            if($request->input('_token')){
                $edit=Groups::edit($request->input());
                if($edit){
                    return redirect()->route('groups_index_path');
                }
                else{
                    $list_groups=Groups::getListId($id);
                    return view('backend.group.add',compact('list_groups'));
                }
            }else{

            }
        }
    }
    public function delete($id){
        $delete=Groups::remove($id);
        if($delete){
            return redirect()->route('groups_index_path');
        }else{
            return redirect()->route('groups_index_path');
        }
    }
}
