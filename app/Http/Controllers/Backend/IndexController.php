<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    protected $layout = "backend.index";
    public function __construct()
    {
        $this->middleware('auth');
        $this->data = $this->setupLayout();
    }
    public function index(){
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "home",
            "method" => "dashboard"
        );
        return view('backend.index',compact('data','structure_info'));
    }

}
