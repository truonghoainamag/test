<?php

namespace App\Http\Controllers\Backend;

use App\Model\Modules;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ModulesController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
        $this->data = $this->setupLayout();
    }
    public function index(){
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "module",
            "method" => "dashboard"
        );
        $list_modules=Modules::getList();
        return view('backend.modules.index',compact('list_modules','data','structure_info'));
    }
    public function create(Request $request){
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "home",
            "method" => "dashboard"
        );
        if($_SERVER['REQUEST_METHOD']=='GET'){
            return view('backend.modules.add',compact('data','structure_info'));
        }else{
            if($request->input('_token')){
                $insert=Modules::insert($request->input());
                if($insert){
                    return redirect()->route('modules_index_path');
                }
                else{
                    return redirect()->route('modules_add_path');
                }
            }else{

            }

        }
    }
    public function edit(Request $request,$id){
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "home",
            "method" => "dashboard"
        );
        if($_SERVER['REQUEST_METHOD']=='GET'){
            $list_modules=Modules::getListId($id);
            return view('backend.modules.add',compact('list_modules','data','structure_info'));
        }else{
            if($request->input('_token')){
                $edit=Modules::edit($request->input());
                if($edit){
                    return redirect()->route('modules_index_path');
                }
                else{
                    $list_modules=Modules::getListId($id);
                    return view('backend.modules.add',compact('list_modules'));
                }
            }else{

            }
        }
    }
    public function delete($id){
        $delete=Modules::remove($id);
        if($delete){
            return redirect()->route('modules_index_path');
        }else{
            return redirect()->route('modules_index_path');
        }
    }
}
