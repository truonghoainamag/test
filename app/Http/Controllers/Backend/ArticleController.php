<?php

namespace App\Http\Controllers\Backend;

use App\Helpers\Helpers;
use App\Model\Article;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Category;
class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $layout = "article.index";
    protected $data = array();
    protected $helpers;
    public function __construct()
    {
        $this->middleware('auth');
        $this->data = $this->setupLayout();
        $this->helpers=new Helpers();
    }
    public function index()
    {
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "home",
            "method" => "dashboard"
        );
        $article=Article::getList();
        return view('backend.article.index',compact('article','data','structure_info'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "home",
            "method" => "dashboard"
        );
        $category=Category::getList();
        return view('backend.article.add',compact('category','data','structure_info'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($_SERVER['REQUEST_METHOD']=='POST'){
            $file=$request->file('images');
            if($file !=''){
                $file_name  =time().'.'.$file->getClientOriginalName();
                $file->move('../public/images/article/',$file_name);
            }else{
                $file_name='';
            }
            $data=array(
                'article_name'=>(isset($request['name']) && !empty($request['name'])) ? $request['name'] : '',
                'article_title'=>(isset($request['title']) && !empty($request['title'])) ? $request['title'] : '',
                'article_category'=>$request['category'],
                'article_status'=>(isset($request['status']) && !empty($request['status'])) ? 1 : 0,
                'article_content'=>(isset($request['content']) && !empty($request['content'])) ? $request['content'] : '',
                'article_image'=>$file_name,
                'article_order'=>$request['order'],
                'article_video'=>(isset($request['article_video']) && !empty($request['article_video'])) ? $request['article_video'] : '',
                'article_seo'=>(isset($request['seo']) && !empty($request['seo'])) ? $this->helpers->utf8convert($request['seo']) : '',
                'article_keywords'=>(isset($request['keywords']) && !empty($request['keywords'])) ? $this->helpers->utf8convert($request['keywords']) : '',
                'article_description'=>(isset($request['description']) && !empty($request['description'])) ? $request['description'] : '',
                'article_hot'=>(isset($request['hot']) && !empty($request['hot'])) ? 1 : 0,
                'article_created_at'=>date('Y/m/d H:i:s',time()),
                'article_updated_at'=>date('Y/m/d H:i:s',time())
            );
            $insert=Article::insert($data);
            if($insert){
                return redirect()->route('article_index_path');
            }
            else{
                return redirect()->route('article_create_path');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "home",
            "method" => "dashboard"
        );
        $article=Article::getListId($id);
        $category=Category::getList();
        return view('backend.article.edit',compact('category','article','data','structure_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($_SERVER['REQUEST_METHOD']=='POST'){
            $file=$request->file('images');
            if($file !=''){
                $file_name  = time().'.'.$file->getClientOriginalName();
                $file->move('../public/images/article/',$file_name);
            }else{
                $file_name = $request['photo'];
            }
            $data=array(
                'article_id'=>$id,
                'article_name'=>(isset($request['name']) && !empty($request['name'])) ? $request['name'] : '',
                'article_title'=>(isset($request['title']) && !empty($request['title'])) ? $request['title'] : '',
                'article_category'=>$request['category'],
                'article_status'=>(isset($request['status']) && !empty($request['status'])) ? 1 : 0,
                'article_content'=>(isset($request['content']) && !empty($request['content'])) ? $request['content'] : '',
                'article_image'=>$file_name,
                'article_order'=>$request['order'],
                'article_video'=>(isset($request['article_video']) && !empty($request['article_video'])) ? $request['article_video'] : '',
                'article_seo'=>(isset($request['seo']) && !empty($request['seo'])) ? $this->helpers->utf8convert($request['seo']) : '',
                'article_keywords'=>(isset($request['keywords']) && !empty($request['keywords'])) ? $this->helpers->utf8convert($request['keywords']) : '',
                'article_description'=>(isset($request['description']) && !empty($request['description'])) ? $this->helpers->utf8convert($request['description']) : '',
                'article_hot'=>(isset($request['hot']) && !empty($request['hot'])) ? 1 : 0,
                'article_created_at'=>date('Y/m/d H:i:s',time()),
                'article_updated_at'=>date('Y/m/d H:i:s',time())
            );
            $insert=Article::edit($data);
            if($insert){
                return redirect()->route('article_index_path');
            }
            else{
                return redirect()->route('article_edit_path');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=Article::remove($id);
        if($delete){
            return redirect()->route('article_index_path');
        }else{
            return redirect()->route('article_index_path');
        }
    }
}
