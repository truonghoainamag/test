<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Model\Users;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class UsersController extends Controller
{
    protected $layout="user.index";
    public function __construct()
    {
        $this->middleware('auth');
        $this->data = $this->setupLayout();
    }
    public function index(){
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "user",
            "method" => "dashboard"
        );
        $list_uesers=Users::getList();
        return view('backend.user.index',compact('list_uesers','data','structure_info'));
    }
    public function create(Request $request){
        if($_SERVER['REQUEST_METHOD']=='GET'){
            return view('backend.user.add');
        }else{
            if($request->input('_token')){
                $insert=Groups::insert($request->input());
                if($insert){
                    return redirect()->route('groups_index_path');
                }
                else{
                    return redirect()->route('groups_add_path');
                }
            }else{

            }

        }
    }
    public function edit(Request $request,$id){
        if($_SERVER['REQUEST_METHOD']=='GET'){
            $list_user=Users::getListId($id);
            return view('backend.user.add',compact('list_user'));
        }else{
            if($request->input('_token')){
                $edit=Users::edit($request->input());
                if($edit){
                    return redirect()->route('groups_index_path');
                }
                else{
                    $list_user=Users::getListId($id);
                    return view('backend.group.add',compact('list_user'));
                }
            }else{

            }
        }
    }
    public function delete($id){
        $delete=Groups::remove($id);
        if($delete){
            return redirect()->route('groups_index_path');
        }else{
            return redirect()->route('groups_index_path');
        }
    }
}
