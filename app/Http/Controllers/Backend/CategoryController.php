<?php

namespace App\Http\Controllers\Backend;

use App\Model\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \App\Helpers\Helpers as Helpers;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $layout = "category.index";
    protected $data = array();
    protected $helpers;
    public function __construct()
    {
        $this->middleware('auth');
        $this->data = $this->setupLayout();
        $this->helpers=new Helpers();
    }
    public function index(Request $request)
    {
        $page					= isset($_GET['page']) ? $_GET['page'] : 1;
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "category",
            "method" => "dashboard"
        );
        $data['url_param']='';
        $data['total'] = 20;
        $where=array();
        $search='';
        if(isset($request['search'])){
          $where=array('category_name'=>$request['search']);
            $search=$request['search'];
        }
        $category = Category::getList($where);
        foreach ($category as $cate){
            $cate_parent[]=Category::getListId($cate['category_parent']);
        }
        return view('backend.category.index',compact('category','search','data','structure_info','page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "category",
            "method" => "dashboard"
        );
       $category=Category::getListAll();

       $categorys=$this->helpers->backend_menu(0,$category);
       return view('backend.category.add',compact('categorys','data','structure_info'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=array(
            'category_name'=>$request['name'],
            'category_order'=>$request['order'],
            'category_position'=>$request['position'],
            'category_parent'=>$request['parent'],
            'category_status'=>isset($request['status']) ? $request['status'] : 0,
            'category_seo'=>(isset($request['seo']) && !empty($request['seo'])) ? $this->helpers->utf8convert($request['seo']) : '',
            'category_keywords'=>(isset($request['keywords']) && !empty($request['keywords'])) ? $this->helpers->utf8convert($request['keywords']) : '',
            'category_description'=>(isset($request['description']) && !empty($request['description'])) ? $this->helpers->utf8convert($request['description']) : '',
        );
        $category=Category::insert($data);
        if ($category){
            return redirect()->route('category_index_path');
        }else{
            return redirect()->route('category_create_path');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "category",
            "method" => "dashboard"
        );
        $category=Category::getListId($id);
        $list_category=Category::getListAll();
        $categories=$this->helpers->backend_menu(0,$list_category);
        return view('backend.category.edit',compact('category','categories','data','structure_info'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data=array(
            'id'=>$request['id'],
            'category_name'=>$request['name'],
            'category_order'=>$request['order'],
            'category_position'=>$request['position'],
            'category_parent'=>$request['parent'],
            'category_status'=>isset($request['status']) ? $request['status'] : 0,
            'category_seo'=>(isset($request['seo']) && !empty($request['seo'])) ? $this->helpers->utf8convert($request['seo']) : '',
            'category_keywords'=>(isset($request['keywords']) && !empty($request['keywords'])) ? $this->helpers->utf8convert($request['keywords']) : '',
            'category_description'=>(isset($request['description']) && !empty($request['description'])) ? $this->helpers->utf8convert($request['description']) : '',
        );
        $category=Category::edit($data);
        if ($category){
            return redirect()->route('category_index_path');
        }else{
            return redirect()->route('category_edit_path');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete=Category::remove($id);
        if($delete){
            return redirect()->route('category_index_path');
        }else{
            return redirect()->route('category_index_path');
        }
    }
}
