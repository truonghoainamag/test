<?php

namespace App\Http\Controllers\Backend;

use App\Model\Groups;
use App\Model\Modules;
use App\Model\Permissions;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PermissionsController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
        $this->data = $this->setupLayout();
    }
    public function index(){
        $data['menu'] = $this->data['menu'];
        $data['menu_action'] = $this->data['menu_action'];
        $structure_info = array(
            "name" => "permissions",
            "method" => "dashboard"
        );
        $list_group=Groups::getList();
        $list_module=Modules::getList();
        $list_permissions=Permissions::getList();
        foreach($list_permissions as $per){
            $data['permission'][$per['module_id']] = $per;
        }
        return view('backend.permissions.index',compact('data','list_group','list_module','data','structure_info'));
    }
    public function add(Request $request){
        if($_SERVER['REQUEST_METHOD']=='GET'){
            return view('backend.permissions.add',compact('list_group','list_module'));
        }else{
            if($request->input('_token')){
                $data=$request->input('checkbox');
                $action='';
                foreach ($data as $key =>$value){
                    foreach ($value as $k=>$v){
                        $action.=','.$k;
                    }
                    $list_permission=Permissions::getList(array('group_id'=>$request->input('group_id'),'module_id'=>$key));
                    if(count($list_permission) > 0){
                        $data=array(
                            'action'=>substr($action,1,500),
                            'group_id'=>$request->input('group_id'),
                            'module_id'=>$key,
                        );
                        $update=Permissions::edit($data);
                    }else{
                        $data=array(
                            'action'=>substr($action,1,500),
                            'group_id'=>$request->input('group_id'),
                            'module_id'=>$key,
                        );
                        $insert=Permissions::insert($data);
                    }
                }
                return redirect()->route('permissions_index_path');
            }
        }
    }
    public function edit(Request $request,$id){
        if($_SERVER['REQUEST_METHOD']=='GET'){
            $list_permissions=Permissions::getListId($id);
            return view('backend.permissions.add',compact('list_permissions'));
        }else{
            if($request->input('_token')){
                $edit=Permissions::edit($request->input());
                if($edit){
                    return redirect()->route('permissions_index_path');
                }
                else{
                    $list_permissions=Permissions::getListId($id);
                    return view('backend.permissions.add',compact('list_permissions'));
                }
            }else{

            }
        }
    }
    public function delete($id){
        $delete=Permissions::remove($id);
        if($delete){
            return redirect()->route('permissions_index_path');
        }else{
            return redirect()->route('permissions_index_path');
        }
    }
}
