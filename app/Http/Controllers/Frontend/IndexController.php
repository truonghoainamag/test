<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Helpers;
use App\Model\Article;
use App\Model\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public $helpers;
    public $html;
    public function __construct()
    {
        $this->helpers=new Helpers();
    }
    public function index(){
        $this->html=$this->helpers->frontend_menu();
        $list_article=Article::getListView();
        $list_article_new=Article::getList(array('article_hot'=>1));
        $category_right=Category::getList(array('category_position'=>3));
        $category_left=Category::getList(array('category_position'=>2));
        $list_article_top=Article::getListReadTop();
        return view('frontend.index',['html'=>$this->html,'list_article'=>$list_article,'category_right'=>$category_right,'category_left'=>$category_left,'list_article_new'=>$list_article_new,'list_article_top'=>$list_article_top]);
    }
}
