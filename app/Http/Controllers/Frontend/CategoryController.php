<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Helpers;
use App\Model\Article;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public $helpers;
    public $html;
    public function __construct()
    {
        $this->helpers=new Helpers();
    }

    public function index($id){
        $this->html=$this->helpers->frontend_menu();
        $cateArticle=Article::getListCategory($id);
        $category=Category::getListId($id);
        $category_right=Category::getList(array('category_position'=>3));
        $category_left=Category::getList(array('category_position'=>2));
        $list_article_new=Article::getList(array('article_hot'=>1));
        $list_article_top=Article::getListReadTop();
        return view('frontend.category',['cateArticle'=>$cateArticle,'html'=>$this->html,'category'=>$category,'category_right'=>$category_right,'category_left'=>$category_left,'list_article_new'=>$list_article_new,'list_article_top'=>$list_article_top]);
    }
}
