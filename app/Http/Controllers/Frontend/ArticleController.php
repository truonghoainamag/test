<?php

namespace App\Http\Controllers\Frontend;

use App\Helpers\Helpers;
use App\Model\Article;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Category;
class ArticleController extends Controller
{
    public $helpers;
    public $html;
    public function __construct()
    {
        $this->helpers=new Helpers();
    }
    public function index($id){
        $this->html=$this->helpers->frontend_menu();
        $list_article_detail=Article::getListId($id);
        $category_right=Category::getList(array('category_position'=>3));
        $category_left=Category::getList(array('category_position'=>2));
        $list_article_new=Article::getList(array('article_hot'=>1)); 
        $list_article_top=Article::getListReadTop();
        return view('frontend.article_detail',['list_article_detail'=>$list_article_detail,'html'=>$this->html,'category_right'=>$category_right,'category_left'=>$category_left,'list_article_new'=>$list_article_new,'list_article_top'=>$list_article_top]);
    }
    public function search(Request $request){
        $this->html=$this->helpers->frontend_menu();
        $search=array();
        if($request->all()){
            $search['article_name']=$request['search'];
        }
        $category_right=Category::getList(array('category_position'=>3));
        $category_left=Category::getList(array('category_position'=>2));
        $list_search=Article::getListView($search);
        $list_article_new=Article::getList(array('article_hot'=>1));
        $list_article_top=Article::getListReadTop();
        return view('frontend.search',['list_search'=>$list_search,'html'=>$this->html,'search'=>$request['search'],'category_right'=>$category_right,'category_left'=>$category_left,'list_article_new'=>$list_article_new,'list_article_top'=>$list_article_top]);
    }
}
