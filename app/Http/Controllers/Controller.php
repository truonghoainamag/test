<?php

namespace App\Http\Controllers;

use App\Model\Modules;
use App\Model\Permissions;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Session;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected $layout = 'backend.index';
    protected function setupLayout($router = NULL)
    {
        $layouts = array();
        $module = array();
        $user = Session::get('user');
        $layouts['menu'] = Permissions::getMenu($user);
        $layouts['module'] =Modules::listAllModule()['list'];
        foreach($layouts['module'] as $mod)
        {
            $module[$mod['action_menu']][] = $mod;
        }
        $layouts['menu_action'] = $module;
        return $layouts;
    }
}
