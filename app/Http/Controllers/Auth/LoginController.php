<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Session;
use Cookie;
use Redirect;
use App\Model\Users;
class LoginController extends Controller
{
    public function login(){
        return view('backend.auth.login');
    }
    public function logout() {
        $user = Session::get('user');
        if(isset($user)){
            Session::flush();
            Session::forget('user');
            Cookie::queue(Cookie::forget('laravel_value'));
            return Redirect::to('admin/');
        } else{
            return Redirect::to('admin/');
        }
    }
    public function run(LoginRequest $request) {
        if ($_SERVER['REQUEST_METHOD'] == "POST")
        {
            $user = Session::get('user');
            if(!isset($user))
            {
                $data = $request->input();
                $user =Users::login($data);
                if($user && ($user['status'] == 1))
                {
                    $email = $user['email'];
                    $userdata = array(
                        'user_id'   => $user['user_id'],
                        'username'  => $user['username'],
                        'email'     => $email,
                        'fullname'  => $user['fullname'],
                        'group_id'  => $user['group_id'],
                        'token'     => 'bdx4OsrmB8cPlghUiXklD6dnWkvDFMsALupqo0Bj',
                    );
                    Session()->regenerate();
                    Session::put('user', $userdata);
                    Cookie::queue(Cookie::make('laravel_value', sha1($email), 259200));
                }
                return Redirect()->route('home_index_path');
            }
            else{
                return Redirect()->route('login_index_path');
            }
        }
    }
}
