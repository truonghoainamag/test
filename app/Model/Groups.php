<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    public $table = 'groups';
    public $timestamps = false;
    public $fillable=[
        'group_id',
        'name',
        'status',
        'created_date',
        'last_update',
    ];
    public static function getList($param=array()){
        if(!empty($param)){
            $list=Groups::where(function ($query) use($param){
                foreach($param as $k=>$v){
                    $query->where($k,'=',$v);
                }
            })
            ->select('*')->paginate(20);
        }else{
            $list=Groups::select('*')->paginate(5);
        }
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListId($id){
        $list=Groups::where('group_id','=',$id)->select('*')->get();
        if(count($list)>0){
            return $list->toArray()[0];
        }else{
            return array();
        }
    }
    public static function insert($param){
        $data=array(
            'name'=>$param['name'],
            'status'=>isset($param['name']) ? $param['name'] : 0,
            'created_date'=>date('Y-m-d H:i:s',time()),
        );
        $insert=Groups::create($data);
        return $insert;
    }
    public static function edit($param){
        $data=array(
            'name'=>$param['name'],
            'status'=>isset($param['status']) ? $param['status'] : 0,
            'last_update'=>date('Y-m-d H:i:s',time()),
        );
        $edit=Groups::where('group_id',$param['id'])->update($data);
        return $edit;
    }
    public static function remove($id){
        $delete = Groups::where('group_id', $id)->delete();
        return $delete;
    }
}
