<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    public $table = 'user';
    public $timestamps = false;
    public $fillable=[
        'user_id',
        'username',
        'fullname',
        'group_id',
        'name',
        'status',
        'created_date',
        'last_update',
    ];
    public static function getList($param=array()){
        if(!empty($param)){
            $list=Users::where(function ($query) use($param){
                foreach($param as $k=>$v){
                    $query->where($k,'=',$v);
                }
            })
                ->select('*')->paginate(20);
        }else{
            $list=Users::select('*')->paginate(5);
        }
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListId($id){
        $list=Users::where('group_id','=',$id)->select('*')->get();
        if(count($list)>0){
            return $list->toArray()[0];
        }else{
            return array();
        }
    }
    public static function insert($param){
        $data=array(
            'name'=>$param['name'],
            'status'=>isset($param['name']) ? $param['name'] : 0,
            'created_date'=>date('Y-m-d H:i:s',time()),
        );
        $insert=Users::create($data);
        return $insert;
    }
    public static function edit($param){
        $data=array(
            'name'=>$param['name'],
            'status'=>isset($param['status']) ? $param['status'] : 0,
            'last_update'=>date('Y-m-d H:i:s',time()),
        );
        $edit=Users::where('group_id',$param['id'])->update($data);
        return $edit;
    }
    public static function remove($id){
        $delete = Users::where('group_id', $id)->delete();
        return $delete;
    }
    public static function login($data) {
        $user = Users::where('password','=',md5(($data['password'])))
            ->where('username','=',addslashes($data['username']))
            ->select('*')
            ->first();
        if ($user == NULL) {
            return false;
        } else {
            return $user->toArray();
        }
    }
}
