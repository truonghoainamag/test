<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $table = 'category';
    public $timestamps = false;
    public $fillable=[
        'category_id',
        'category_name',
        'category_position',
        'category_order',
        'category_parent',
        'category_status',
        'category_seo',
        'category_keywords',
        'category_description',
        'category_created_at',
        'category_updated_at',
    ];
    public static function getList($where=array()){

        if(!empty($where)){
            $list=Category::where(function ($query) use($where){
                foreach($where as $k=>$v){
                    $query->where($k,'like','%'.$v.'%');
                }
            })
            ->select('*')->paginate(20);
        } else{
            $list=Category::select('*')->paginate(20);
        }
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListAll($where=array()){
        if(!empty($where)){
            $list=Category::where(function ($query) use($where){
                foreach($where as $k=>$v){
                    $query->where($k,'=',$v);
                }
            })
                ->select('*')->get();
        } else{
            $list=Category::select('*')->get();
        }
        if(count($list)>0){
            return $list->toArray();
        }else{
            return array();
        }
    }
    public static function getListId($id){
        $list=Category::where('category_id','=',$id)->select('*')->get();
        if(count($list)>0){
            return $list->toArray()[0];
        }else{
            return array();
        }
    }
    public static function insert($param){
        $data=array(
            'category_name'=>$param['category_name'],
            'category_order'=>$param['category_order'],
            'category_position'=>$param['category_position'],
            'category_parent'=>$param['category_parent'],
            'category_status'=>isset($param['category_status']) ? $param['category_status'] : 0,
            'category_seo'=>$param['category_seo'],
            'category_keywords'=>$param['category_keywords'],
            'category_description'=>$param['category_description'],
            'category_created_at'=>date('Y-m-d H:i:s',time()),
        );
        $insert=Category::create($data);
        return $insert;
    }
    public static function edit($param){
        $data=array(
            'category_name'=>$param['category_name'],
            'category_order'=>$param['category_order'],
            'category_position'=>$param['category_position'],
            'category_parent'=>$param['category_parent'],
            'category_status'=>isset($param['category_status']) ? $param['category_status'] : 0,
            'category_seo'=>$param['category_seo'],
            'category_keywords'=>$param['category_keywords'],
            'category_description'=>$param['category_description'],
            'category_updated_at'=>date('Y-m-d H:i:s',time()),
        );
        $edit=Category::where('category_id',$param['id'])->update($data);
        return $edit;
    }
    public static function remove($id){
        $delete = Category::where('category_id', $id)->delete();
        return $delete;
    }
}
