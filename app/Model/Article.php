<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;
class Article extends Model
{
    public $table = 'article';
    public $timestamps = false;
    public $fillable=[
        'article_id',
        'article_name',
        'article_title',
        'article_image',
        'article_content',
        'article_category',
        'article_status',
        'article_order',
        'article_hot',
        'article_view',
        'article_seo',
        'article_keywords',
        'article_description',
        'article_slider',
        'article_video',
        'article_created_at',
        'article_updated_at'
    ];
    public static function getList($param=''){
        if(!empty($param)){
            $list=Article::where(function ($query) use($param){
                foreach($param as $k=>$v){
                    $query->where($k,'like','%'.$v.'%');
                }
            })
                ->select('*')->paginate(20);
        }else{
            $list=Article::select('*')->paginate(20);
        }
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListView($param=''){
        $url_param='tim-kiem?search';
        if(!empty($param)){
            $list=Article::where(function ($query) use($param){
                foreach($param as $k=>$v){
                    $query->where($k,'like','%'.$v.'%');
                }
            })
                ->select('*')->paginate(20);
            $list->setPath('tim-kiem?search='.$param['article_name']);
        }else{
            $list=Article::select('*')->paginate(20);
        }
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListId($id){
        $list=Article::where('article_id','=',$id)->select('*')->get();
        if(count($list)>0){
            return $list->toArray();
        }else{
            return array();
        }
    }
    public static function getListCategory($id){
        $list=Article::where('article_category','=',$id)->select('*')->paginate(20);
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListReadTop(){
        $list=Article::select('*')->skip(0)->take(5)->orderBy(DB::raw('RAND()'))->orderBy('article_view','DESC')->get();
        if(count($list)>0){
            return $list->toArray();
        }else{
            return array();
        }
    }
    public static function insert($param){
        $data=array(
            'article_name'=>$param['article_name'],
            'article_title'=>$param['article_title'],
            'article_category'=>$param['article_category'],
            'article_status'=>$param['article_status'],
            'article_content'=>$param['article_content'],
            'article_image'=>$param['article_image'],
            'article_order'=>$param['article_order'],
            'article_seo'=>$param['article_seo'],
            'article_keywords'=>$param['article_keywords'],
            'article_description'=>$param['article_description'],
            'article_hot'=>$param['article_hot'],
            'article_video'=>$param['article_video'],
            'article_created_at'=>$param['article_created_at'],
            'article_updated_at'=>$param['article_updated_at'],
        );
        $insert=Article::create($data);
        return $insert;
    }
    public static function edit($param){
        $data=array(
            'article_name'=>$param['article_name'],
            'article_title'=>$param['article_title'],
            'article_category'=>$param['article_category'],
            'article_status'=>$param['article_status'],
            'article_content'=>$param['article_content'],
            'article_image'=>$param['article_image'],
            'article_order'=>$param['article_order'],
            'article_seo'=>$param['article_seo'],
            'article_keywords'=>$param['article_keywords'],
            'article_description'=>$param['article_description'],
            'article_hot'=>$param['article_hot'],
            'article_video'=>$param['article_video'],
            'article_created_at'=>$param['article_created_at'],
            'article_updated_at'=>$param['article_updated_at'],
        );
        $edit=Article::where('article_id',$param['article_id'])->update($data);
        return $edit;
    }
    public static function remove($id){
        $delete = Article::where('article_id', $id)->delete();
        return $delete;
    }
    public static function getListNew(){
        $list=Article::select('*')->skip(0)->take(5)->orderBy(DB::raw('RAND()'))->get()->toArray();
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListSlider(){
        $list=Article::select('*')->where('article_slider','=',1)->orderBy(DB::raw('RAND()'))->get()->toArray();
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListSliderContent(){
        $list=Article::select('*')->skip(0)->take(5)->orderBy(DB::raw('RAND()'))->get()->toArray();
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListCommon(){
        $list=Article::select('*')->skip(0)->take(10)->orderBy(DB::raw('RAND()'))->get()->toArray();
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
}
