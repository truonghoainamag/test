<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    public $table = 'permissions';
    public $timestamps = false;
    public $fillable=[
        'permission_id',
        'group_id',
        'module_id',
        'status',
        'action',
        'created_date',
        'last_update',
    ];
    public static function getList($param=array()){
        if(!empty($param)){
            $list=Permissions::where(function ($query) use($param){
                foreach($param as $k=>$v){
                    $query->where($k,'=',$v);
                }
            })
                ->select('*')->get();
        }else{
            $list=Permissions::select('*')->get();
        }
        if(count($list)>0){
            return $list->toArray();
        }else{
            return array();
        }
    }
    public static function getListId($id){
        $list=Permissions::where('permission_id','=',$id)->select('*')->get();
        if(count($list)>0){
            return $list->toArray()[0];
        }else{
            return array();
        }
    }
    public static function insert($param){

        $data=array(
            'group_id'=>$param['group_id'],
            'module_id'=>isset($param['module_id']) ? $param['module_id']: '',
            'action'=>isset($param['action']) ? $param['action']: '',
            'created_date'=>date('Y-m-d H:i:s',time()),
        );

        $insert=Permissions::create($data);
        return $insert;
    }
    public static function edit($param){
        $data=array(
            'action'=>$param['action'],
            'status'=>1,
            'last_update'=>date('Y-m-d H:i:s',time()),
        );
        $edit=Permissions::where('group_id','=',$param['group_id'])->where('module_id','=',$param['module_id'])->update($data);
        return $edit;
    }
    public static function remove($id){
        $delete = Permissions::where('permission_id', $id)->delete();
        return $delete;
    }
    public static function checkPermission($user, $module, $action){
        $group_id = $user['group_id'];
        $permission = Permissions::join('modules', 'modules.module_id', '=', 'permissions.module_id')
            ->where('permissions.action', 'LIKE', "%" . $action . "%")
            ->where('permissions.group_id', $group_id)
            ->where('modules.name', ucfirst($module))
            ->select('*')
            ->first();

        if($permission == NULL){
            return array();
        } else{
            return $permission->toArray();
        }
    }
    public static function getMenu($user){
        $group_id = $user['group_id'];
        $permission = Permissions::join('modules','modules.module_id', '=', 'permissions.module_id')
            ->where('permissions.group_id', $group_id)
            ->select('permissions.*', 'modules.name as module_name', 'modules.action_menu as action_menu')
            ->get();
        if($permission == NULL){
            return array();
        } else{
            return $permission->toArray();
        }
    }
}
