<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    public $table = 'modules';
    public $timestamps = false;
    public $fillable=[
        'module_id',
        'name',
        'action',
        'show_menu',
        'action_menu',
        'icon',
        'status',
        'created_date',
        'last_update',
    ];
    public static function getList($param=array()){
        if(!empty($param)){
            $list=Modules::where(function ($query) use($param){
                foreach($param as $k=>$v){
                    $query->where($k,'=',$v);
                }
            })
                ->select('*')->paginate(20);
        }else{
            $list=Modules::select('*')->paginate(20);
        }
        if(count($list)>0){
            return $list;
        }else{
            return array();
        }
    }
    public static function getListId($id){
        $list=Modules::where('module_id','=',$id)->select('*')->get();
        if(count($list)>0){
            return $list->toArray()[0];
        }else{
            return array();
        }
    }
    public static function insert($param){
        $data=array(
            'name'=>$param['name'],
            'action'=>$param['action'],
            'show_menu'=>$param['show_menu'],
            'action_menu'=>$param['action_menu'],
            'icon'=>$param['icon'],
            'status'=>isset($param['status']) ? $param['status'] : 0,
            'created_date'=>date('Y-m-d H:i:s',time()),
            'last_update'=>date('Y-m-d H:i:s',time()),
        );
        $insert=Modules::create($data);
        return $insert;
    }
    public static function edit($param){
        $data=array(
            'name'=>$param['name'],
            'action'=>$param['action'],
            'show_menu'=>$param['show_menu'],
            'action_menu'=>$param['action_menu'],
            'icon'=>$param['icon'],
            'status'=>isset($param['status']) ? $param['status'] : 0,
            'order'=>isset($param['order']) ? $param['order'] : 0,
            'last_update'=>date('Y-m-d H:i:s',time()),
        );
        $edit=Modules::where('module_id',$param['id'])->update($data);
        return $edit;
    }
    public static function remove($id){
        $delete = Modules::where('module_id', $id)->delete();
        return $delete;
    }
    public static function listAllModule(){

        $user = Modules::orderBy('module_id', 'asc')->get();

        if($user == NULL){
            return array();
        } else{
            $data = array(
                'list' => $user->toArray(),
            );
            return $data;
        }
    }
}
