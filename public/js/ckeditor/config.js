/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.filebrowserBrowseUrl = 'http://dev.laravel.com/js/ckfinder/ckfinder.html';

    config.filebrowserImageBrowseUrl = 'http://dev.laravel.com/js/ckfinder/ckfinder.html?type=Images';

    config.filebrowserFlashBrowseUrl = 'http://dev.laravel.com/js/ckfinder/ckfinder.html?type=Flash';

    config.filebrowserUploadUrl = 'http://dev.laravel.com/ckfinder/js/core/connector/php/connector.php?command=QuickUpload&type=Files';

    config.filebrowserImageUploadUrl = 'http://dev.laravel.com/ckfinder/js/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserFlashUploadUrl = 'http://dev.laravel.com/ckfinder/js/core/connector/php/connector.php?command=QuickUpload&type=Flash';

};
